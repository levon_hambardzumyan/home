<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gs_contestpeople;

use DB;
use Session;

class IndexController extends Controller
{
    public function index()
    {
        $members = DB::table('gs_contestpeople')
            ->orderBy('score', 'desc')
            ->take(10)
            ->get();
        return view('home', array('members' => $members));
    }

    public function members(Request $request)
    {
        $query = DB::table('gs_contestpeople');
        $search = '';
        if ($request->has('search_res')) {
            $search = $request->input('search_res');
            $query->where('whois', 'LIKE', '%' . $search . '%');
        }

        $members = $query->paginate(12);
        $members->appends([
            'search_res' => $search
        ]);
        $likes = Session::get('likes', array());
        if ($request->ajax()) {
            return view('members-ajax', array('members' => $members, 'likes' => $likes));
        } else {
            return view('members', array('members' => $members, 'likes' => $likes, 'search' => $search));
        }

    }

    public function member($id)
    {
        $member = Gs_contestpeople::findOrFail($id);
        return view('member', array('member' => $member));
    }

    public function add($id)
    {
        $likes = Session::get('likes', array());
        if (in_array($id, $likes)) {
            return response()->json([
                'status' => false,
                'message' => 'Извините но вы уже проголосовали'
            ]);
        }
        $member = Gs_contestpeople::findOrFail($id);
        ++$member->score;
        $isSavaed = $member->save();
        if ($isSavaed) {
            $likes[] = $id;
            Session::put('likes', $likes);
        }
        return response()->json([
            'status' => $isSavaed,
            'score' => $member->score
        ]);
    }


}


