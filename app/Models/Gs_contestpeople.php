<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Gs_contestpeople extends Model
{
    protected $table = "gs_contestpeople";
    public  $timestamps = false;
}
