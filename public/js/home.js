/*************** One Page Scroll ***************/

$(document).ready(function(){
  "use strict";
  $(".main").onepage_scroll({
    sectionContainer: "section", 
    responsiveFallback: 767
  });
});

/*************** Magnific-Popup Video ***************/

$(document).ready(function() {
	"use strict";
	$('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
		disableOn: 700,
		type: 'iframe',
		mainClass: 'mfp-fade',
		removalDelay: 160,
		preloader: false,

		fixedContentPos: false
	});
});

/*************** Magnific-Popup Lightbox ***************/

$(document).ready(function() {
	"use strict";
	$('.popup-gallery').magnificPopup({
		delegate: 'a',
		type: 'image',
		tLoading: 'Loading image #%curr%...',
		mainClass: 'mfp-img-mobile',
		gallery: {
			enabled: true,
			navigateByImgClick: true,
			preload: [0,1] // Will preload 0 - before current, and 1 after the current image
		},
		image: {
			tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
			titleSrc: function(item) {
				return item.el.attr('title', 'sss');
			}
		},
		callbacks: {
			elementParse: function(item) {
				// Function will fire for each target element
				// "item.el" is a target DOM element (if present)
				// "item.src" is a source that you may modify

				console.log(item); // Do whatever you want with "item" object
			}
		}
	});
});

/*************** GMaps ***************/

var map;
$(document).ready(function(){
  "use strict";
  map = new GMaps({
    el: '#map',
    lat: 55.78565935620307,
    lng: 37.595858573913574,
    zoom: 17,
    zoomControl : false,
    zoomControlOpt: {
        style : 'SMALL',
        position: 'TOP_LEFT'
    },
    panControl : false,
    streetViewControl : false,
    mapTypeControl: false,
    overviewMapControl: false
  });
  map.addMarker({
    lat: 55.78565935620307,
    lng: 37.595858573913574,
  });
});

/*************** Flexslider ***************/

$(window).load(function(){
  "use strict";
  $('.flexslider').flexslider({
    animation: "slide",
    slideshow: false,
    start: function(slider){
      $('body').removeClass('loading');
    }
  });

    // check for mobile view
    if( $("#navigation").is(":hidden") ) {
        var current_page_section = location.hash.replace(/[^0-9]/g, '');
        current_page_section = parseInt(current_page_section);
        if (current_page_section > 0) {
            $('#select_menu').val(BASE_URL + "/#" + current_page_section).trigger('change');
        }
    }

});


