/*************** Responsive Navigation ***************/

$(document).ready(function(){
    "use strict";
    // Create the dropdown base
    $("<select id='select_menu' onchange='on_mobile_menu_change(this.value)' />").appendTo("#responsive-nav");

    // Create default option "Navigation"
    $("<option />", {
        "selected": "selected",
        "value"   : "0",
        "text"    : "Navigation"

    }).appendTo("#responsive-nav select");

    // Populate dropdown with menu items
    $("#navigation #menu li a").each(function() {
        var el = $(this);
        $("<option />", {
            "value"   : el.attr("href"),
            "text"    : el.text()
        }).appendTo("#responsive-nav select");
    });

});

var members_link = BASE_URL+'/members';
var member_link = BASE_URL+'/member/';
function members_click() {
    window.location.replace(members_link);
}
function on_mobile_menu_change(value) {
    if ('members' == CURRENT_PAGE) {
        window.location.href = value;
        return;
    }
}
var is_members_page = window.location.href.substring(0, members_link.length) === members_link;
var is_member_page = window.location.href.substring(0, member_link.length) === member_link;
if(is_members_page || is_member_page){
    $('#members').parent().siblings().children('a').removeClass('active');
    $('#members').addClass( "active" );
}
if(is_members_page){
    var member_id = location.hash.replace(/[^0-9]/g, '');
    if (member_id) {
        window.location.replace(member_link + member_id);
    }
}