function onLike(user_id) {
    var url = BASE_URL+'/add/' + user_id;
    $.ajax({
        type: 'get',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            if (response.status) {
                $("#popup-score-" + user_id).html(response.score);
                $("#members-score-" + user_id).html(response.score);
                $("img[data-user_id='" + user_id + "']").attr("data-is_liked", 1);
                $("img[data-user_id='" + user_id + "']").attr("data-score", response.score);
                $("#members-like-button-" + user_id).attr("disabled", "disabled");
                $("#members-popup-like-button-" + user_id).attr("disabled", "disabled");
            } else {
                alert(response.message);

            }
        }
    });
}

//search users
$("#serach_form").submit(function (event) {
    event.preventDefault();

    var str = $('#search_form').serialize();
    var url = BASE_URL + '/members?' + str;
    $.ajax({
        type: 'get',
        url: url,
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $('#search-result').html(response);
        }
    });

});

$('.img_modal').on('click', open_modal);

function open_modal(event) {
    var image_src = $(this).attr('src');
    var user_id = $(this).attr('data-user_id');
    var score = $(this).attr('data-score');
    var whois = $(this).attr('data-whois');
    var is_liked = $(this).attr('data-is_liked');
    is_liked = parseInt(is_liked);
    if (is_liked) {
        var button_html = '<button id="members-popup-like-button-' + user_id + '" disabled="disabled" class="like_button">Голосовать</button>';
    } else {
        var button_html = '<button id="members-popup-like-button-' + user_id + '" class="like_button" onclick="onLike(' + user_id + ')">Голосовать</button>';
    }

    var html = ' <div class="container black-popup">' +
                    '<div class="">' +
                        '<div class="seven columns img-side">' +
                            '<img class="" src="' + image_src + '" width="400" height="400">' +
                        '</div>'+
                        '<div class="four columns offset-by-one like-side">' +
                            '<h4>'+ whois +'</h4>' +
                            '<span>Голос: </span><span id="popup-score-' + user_id + '">'+ score +'</span>' +
                            '<div class="clear"></div>'+
                            button_html +
                        '</div>' +
                    '</div>'+
                '</div>';
    $.magnificPopup.open({
        items: {
            src: html, // can be a HTML string, jQuery object, or CSS selector
            type: 'inline'
        }
    });
    location.hash = "#" + user_id;
}
