@extends('layouts.app')
@section('title','GSclinic - Участник конкурса')
@section('my-scripts')
    <script type="text/javascript" src="{{ URL::to ('js/members.js') }}"></script>
    <script>
        var CURRENT_PAGE = 'members';
    </script>
@endsection
@section('my-stylesheets')
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/color/colorful-video.css') }}">
    <style>
        body {
            background-color: #18acca;
        }

        body, html {
            overflow: auto;
        }
        .member-img {
            max-height: 400px;
        }
    </style>
@endsection
@section('content')
    <div class="container" style="margin-top: 75px;">
        <div class="row">
            <div class="container black-popup">
                <div class="seven columns img-side">
                    <img class="member-img" src="{{asset('users/')}}/{{$member->image}}" width="100%">
                </div>
                <div class="four columns offset-by-one like-side">
                    <h4>{{$member->whois}}</h4>
                    <span>Голос: </span><span id="popup-score" >{{$member->score}}</span>
                    <div class="clear"></div>
                        <button class="like_button" onclick="onLike({{$member->id}})">Голосовать</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
