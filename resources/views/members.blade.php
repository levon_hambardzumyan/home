@extends('layouts.app')
@section('title','GSclinic - Список участников конкурса')
@section('my-scripts')
    <script type="text/javascript" src="{{ URL::to ('js/members.js') }}"></script>
    <script>
        var CURRENT_PAGE = 'members';
    </script>
@endsection
@section('my-stylesheets')
    <link rel="stylesheet" href="{{ URL::to ('css/members.css') }}">
@endsection
@section('content')
<div class="wrapper">
    <div class="main onepage-wrapper">
        <div class="container">
            <div id="responsive-nav"><h1 class="logo"><a href="{{ URL::to ('/') }}">GSclinic</a></h1></div>
            <div class="row">
                <div class="nine columns">
                    <form action="{{ url('/members') }}" class="navbar-form" id="search_form">
                        <div class="row">
                            <input type="text" id="members-search-input" class="search-input five columns" name="search_res" value="{{$search}}"/>
                            <button type="submit" class="search-button two columns">Искать</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="row" id="search-result">
                @include('members-ajax')
            </div>
        </div>
    </div>
</div>

@endsection
