<!DOCTYPE html>
<!--[if lt IE 7 ]>
<html class="ie ie6" lang="ru"> <![endif]-->
<!--[if IE 7 ]>
<html class="ie ie7" lang="ru"> <![endif]-->
<!--[if IE 8 ]>
<html class="ie ie8" lang="ru"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="ru"> <!--<![endif]-->

<head>
    <!-- Basic Page Needs
================================================== -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title')</title>
    <meta name="description"
          content="Официальная страница пластического хирурга Геворга Хореновича Степаняна, GSclinic App">
    <meta name="keywords"
          content="plastic surgery, moscow, gevorg stepanyan, mobile app, пластическая хирургия, москва, геворг хоренович, мобильное приложение, ринопластика, rhinoplasty">
    <meta name="author" content="iMed Solutions, Hrachya Aeshakyan">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- CSS
	================================================== -->
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/base.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/skeleton.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/layout.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/onepage-scroll.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/magnific-popup.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/video-js.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/flexslider.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/color/colorful-video.css') }}">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/color/blue.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::to ('stylesheets/color/white-text.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ URL::to ('css/style.css') }}">
    @yield('my-stylesheets')

    <!-- Favicons
    ================================================== -->
    <link rel="shortcut icon" href="{{ URL::to ('images/favicon.ico') }}">
    <link rel="apple-touch-icon" href="{{ URL::to ('images/apple-touch-icon.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ URL::to ('images/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::to ('images/apple-touch-icon-114x114.png') }}">

    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script>
        var BASE_URL = "{{ url ('/') }}";
    </script>
</head>
<body>
<nav id="navigation" class="animated1 bounceInDown delay15">
    <div class="container">
        <h1 class="logo"><a href="{{ url ('/') }}/">GSclinic</a></h1>
        <ul id="menu">
            <li><a data-index="1" href="{{ url ('/') }}/#1" class="active">ГЛАВНОЕ</a></li>
            <li><a data-index="2" href="{{ url ('/') }}/#2">СКАЧАТЬ</a></li>
            <li><a data-index="3" href="{{ url ('/') }}/#3">ОБЪЯВЛЕН КОНКУРС</a></li>
            <li><a id="members" onclick="members_click();return false;" href="{{ url ('/') }}/#">УЧАСТНИКИ</a></li>
            <li><a data-index="4" href="{{ url ('/') }}/#4">КАК ЭТО РАБОТАЕТ</a></li>
            <li><a data-index="5" href="{{ url ('/') }}/#5">КОНТАКТЫ</a></li>
        </ul>
    </div>
</nav>

@yield('content')


        <!-- Scripts
================================================== -->
<script type="text/javascript" src="{{ URL::to ('js/jquery-1.10.2.min.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/menu.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/jquery.onepage-scroll.my.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/jquery.magnific-popup.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/video.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/gmapapi.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/gmaps.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/jquery.flexslider.js') }}"></script>
<script type="text/javascript" src="{{ URL::to ('js/modernizr-2.6.2.min.js') }}"></script>

<script>
    if (Modernizr.csstransitions) {
        document.write('<link rel="stylesheet" href="{{ URL::to ('stylesheets/animations.css') }}" />');
    }
</script>

@yield('my-scripts')

</body>
</html>
