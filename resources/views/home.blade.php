@extends('layouts.app')
@section('title','GSclinic - Геворг Хоренович Степанян - Пластическая хирургия в Москве')
@section('my-scripts')
    <script type="text/javascript" src="{{ URL::to ('js/home.js') }}"></script>
    <script>
        var CURRENT_PAGE = 'home';
    </script>
@endsection
@section('my-stylesheets')
@endsection
@section('content')
    <div class="wrapper">
        <div class="main">
            <section id="page1">
                <div class="container">
                    <div id="responsive-nav"><h1 class="logo"><a href="{{ URL::to ('/') }}">GSclinic</a></h1></div>
                    <h1 class="animated05 fadeIn delay2">Геворг Хоренович Степанян</h1>
                    <h3 class="animated05 fadeIn delay2">Официальная страница пластического хирурга</h3>

                    <div id="page1-phones">
                        <img id="first-phone" class="animated15 bounceInUp" src="{{asset('images/phone3_page1.png')}}"
                             alt="phone"/>
                        <img id="second-phone" class="animated15 bounceInUp delay05"
                             src="{{asset('images/phone1_page1.png')}}" alt="phone"/>
                        <img id="third-phone" class="animated15 bounceInUp delay1"
                             src="{{asset('images/phone2_page1.png')}}" alt="phone"/>
                    </div>
                </div>
            </section>

            <section id="page2">
                <div class="container">
                    <img class="phone animated15 bounceInUp" src="{{asset('images/phone_page2.png')}}" alt="phone">
                    <div class="left-side animated15 bounceInDown">
                        <h2>Download for iPhone <br/> and iPad as well</h2>
                        <p class="animated15 fadeIn">Lorem ipsum dolor sit amet, consectetur adipiscing elit. In
                            placerat arcu at dui pretium, in sollicitudin felis pretium. Interdum et malesuada fames ac
                            ante ipsum primis in faucibus. Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
                        <div id="button" class="animated1 flipInX delay1">
                            <a href="">
                                <h4>Available on the App Store</h4>
                                <p>Lorem ipsum dolor amet consecter...</p>
                            </a>
                        </div>
                    </div>
                </div>
            </section>

            <section id="page3">
                <div class="container">
                    <h1 class="animated05 fadeIn delay1">Конкурс - Лучший модель носа!</h1>
                    <p class="animated05 fadeIn delay1">Топ список участников конкурса.</p>
                    <div class="bottom-side animated15 bounceInUp">
                        <div class="flexslider">
                            <ul class="slides popup-gallery">

                                @if(isset($members))
                                    @foreach($members as $member)
                                        <li class="img_score">
                                            <a href="{{URL::to ('users/')}}/{{$member->image}}" >
                                                <img src="{{asset('users/')}}/{{$member->image}}"  alt="Picture">
                                                <en  class="span_score">Голос:{{$member->score}}</en>
                                            </a>
                                        </li>
                                    @endforeach
                                @endif

                            </ul>
                        </div>
                        <img id="tablet" src="{{asset('images/pad_page3.png')}}" alt="phone">
                    </div>
                </div>
            </section>

            <section id="page4">
                <div class="container">

                    <div class="right-side animated15 bounceInUp">
                        <video id="phone-video" class="video-js vjs-default-skin" controls preload="auto" width="279"
                               height="494" poster="{{ URL::to ('images/screen-bigger1@2x.jpg') }}" data-setup="{}">
                            <source src="http://www.sveccdesign.com/preview/applight/v1/versions/1/videos/1.mp4"
                                    type='video/mp4'/>
                            <source src="http://www.sveccdesign.com/preview/applight/v1/versions/1/videos/1.webm"
                                    type='video/webm'/>
                            <source src="http://www.sveccdesign.com/preview/applight/v1/versions/1/videos/1.ogg"
                                    type='video/ogg'/>
                        </video>
                        <img class="phone" src="{{asset('images/phone_page4.png')}}" alt="phone">
                    </div>
                    <div class="left-side animated15 bounceInDown">
                        <h2>Как работает мое <br/> приложение</h2>
                        <p class="animated15 fadeIn">Здесь видео доктора и небольшой текст.</p>
                        <a class="popup-vimeo" href="http://vimeo.com/1084537"><img class="animated1 flipInX delay1"
                                                                                    src="{{asset('images/video.png')}}"
                                                                                    alt="video thumb"/>
                            <h3 class="animated2 fadeIn delay1">ПОСМОТРЕТЬ РОЛИК</h3></a>
                    </div>
                </div>
            </section>

            <section id="page5">
                <div class="container">
                    <form>
                        <label class="animated05 fadeIn delay1" for="mce-EMAIL">
                            <h1 class="animated05 fadeIn delay1">+7 926 1116640</h1>
                            <h3 class="animated05 fadeIn delay1">В мобильном приложение доступна навигационная
                                карта!</h3>
                        </label>
                    </form>
                    <ul>
                        <li class="animated05 rollIn delay1"><a class="facebook"
                                                                href="https://www.facebook.com/GSclinic"
                                                                target="_blank"></a></li>
                        <li class="animated05 rollIn delay15"><a class="linkedin"
                                                                 href="https://www.instagram.com/dr_gevorg_khorenovich/"
                                                                 target="_blank"></a></li>
                    </ul>
                    <h3></h3>
                    <div class="bottom-side animated15 bounceInUp">
                        <div id="map"></div>
                        <img class="phone" src="{{asset('images/phone_page5.png')}}" alt="phone">
                    </div>
                    <p class="animated05 fadeIn delay1">&copy; 2016 <a href="http://imed.eu.com/ru/adviser"
                                                                       target="_blank">iMed Solutions</a> <br/>
                        Разработка мобильных приложений</p>
                </div>
            </section>

        </div>
    </div>
@endsection

