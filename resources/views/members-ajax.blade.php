@if(isset($members))
@foreach($members as $member)
<div class="four columns member-item">
    <div class="member-thumbnail">
        <img class="img_modal" src="{{asset('users/')}}/{{$member->image}}"
             style="cursor:pointer" data-score="{{$member->score}}" data-user_id="{{$member->id}}"
             data-is_liked="{{in_array($member->id, $likes) ? 1 : 0}}" data-whois="{{$member->whois}}"
             alt="" width="100%" max-height="240" />
    </div>
    <div class="caption">
        <h5 style="color:black">{{$member->whois}}</h5>
        <button
               id="members-like-button-{{$member->id}}"
               {{in_array($member->id, $likes) ? "disabled='disabled'" : ''}}
               class="md-button"
               onclick="onLike({{$member->id}})">Голосовать</button>
        <span style="color:black"  id="members-score-{{$member->id}}" >{{$member->score}}</span>
    </div>
</div>
@endforeach
@unless (count($members))
<div class="ten columns">
    <p>По вашему запросу ничего не найдено.</p>
</div>

@endunless
@endif
<div class="clear"></div>
<div align="center" class="text-center"> {{ $members->links() }}</div>